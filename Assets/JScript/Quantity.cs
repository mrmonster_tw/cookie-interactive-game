﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quantity : MonoBehaviour
{
    public int qua;
    public Text qua_text;
    public int anwer;
    public bool check;
    GameObject mainsc;

    // Start is called before the first frame update
    void Start()
    {
        mainsc = GameObject.FindGameObjectWithTag("main");
    }

    // Update is called once per frame
    void Update()
    {
        qua_text.text = qua.ToString();

        if (qua == anwer && check == false)
        {
            mainsc.GetComponent<Main>().answer++;
            check = true;
        }
        if (qua != anwer && check)
        {
            mainsc.GetComponent<Main>().answer--;
            check = false;
        }
    }
    public void minusplus_fn(int xx)
    {
        if (xx == 0 && qua > 0)
        {
            qua = qua - 1;
        }
        if (xx == 1 && qua < 99)
        {
            qua = qua + 1;
        }
        if (xx == 2 && qua > 0)
        {
            qua = qua - 5;
        }
        if (xx == 3 && qua < 1000)
        {
            qua = qua + 5;
        }
    }
}
