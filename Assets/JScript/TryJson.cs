﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TryJson : MonoBehaviour {

	void Start() {
		TestJson();
	}

    // @Leo
    private GameScoreGroup FromJson(string jsonStr) {
        return JsonUtility.FromJson<GameScoreGroup>(jsonStr);
    }

    // @Leo
    private void TestJson() {
        string jsonStr = "{\"data\":\"{\"Games\":[{\"Count\":234,\"GameId\":\"Game8\"},{\"Count\":5,\"GameId\":\"Game9\"},{\"Count\":52,\"GameId\":\"Game7\"}],\"Points\":3}\"}";
		jsonStr = jsonStr.Substring(9, jsonStr.Length - (9 + 2)); // @Leo2
		Debug.Log("new json=" + jsonStr);
		GameScoreGroup resData = FromJson(jsonStr);
        Debug.Log("check! json=" + JsonUtility.ToJson(resData));
    }

	// json第一階data
	[Serializable]
	public class GameScore
	{
		public GameScoreData data;
	}
	// data內三個參數
	[Serializable]
	public class GameScoreData
	{
		public string CardId;
		public string StationId;
		public string GameId;
	}

	// @Leo
	[Serializable]
	public class GameScoreGroup
	{
		public GameScoreData[] Games;
		public int Points;
	}

	// @Leo
	[Serializable]
	public class ResponseData {
		public string data;
	}

}

