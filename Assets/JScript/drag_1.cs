﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drag_1 : MonoBehaviour
{
    public GameObject mainsc;


    // Start is called before the first frame update
    void Start()
    {
        mainsc = GameObject.FindGameObjectWithTag("main");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnMouseDrag()
    {
        if (this.name == "stir_2")
        {
            Vector3 newPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f);
            transform.position = Camera.main.ScreenToWorldPoint(newPosition);
        }
    }
    void OnMouseUp()
    {
        this.transform.localPosition = new Vector3(11,0, 0);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "leftstick")
        {
            mainsc.GetComponent<Main>().stir_left = true;
        }
        if (other.tag == "rightstick")
        {
            mainsc.GetComponent<Main>().stir_right = true;
        }
    }
}
