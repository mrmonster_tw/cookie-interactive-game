﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class foodlink : MonoBehaviour
{
    public GameObject target;
    public GameObject target_1;
    public GameObject mainsc;

    // Start is called before the first frame update
    void Start()
    {
        mainsc = GameObject.FindGameObjectWithTag("main");
    }

    // Update is called once per frame
    void Update()
    {
        if (mainsc.GetComponent<Main>().foodpage.activeSelf)
        {
            if (target == null)
            {
                this.GetComponent<RawImage>().enabled = true;
            }
        }
        if (mainsc.GetComponent<Main>().foodpage_check.activeSelf)
        {
            if (target_1.GetComponent<Quantity>().check)
            {
                this.GetComponent<RawImage>().enabled = true;
            }
            else
            {
                this.GetComponent<RawImage>().enabled = false;
            }
        }
    }
}
