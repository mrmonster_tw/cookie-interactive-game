﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]

public class Drag : MonoBehaviour
{
    public Vector3 orgpos;
    public GameObject mainsc;

    public bool touch;

    // Start is called before the first frame update
    void Start()
    {
        orgpos = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y, this.transform.localPosition.z);
        mainsc = GameObject.FindGameObjectWithTag("main");
    }

    // Update is called once per frame
    void Update()
    {
        if (touch)
        {
            mainsc.GetComponent<Main>().click = false;
            mainsc.GetComponent<Main>().clickob = null;
            if (this.tag == "right")
            {
                mainsc.GetComponent<Main>().chose_count++;
                Destroy(this.gameObject);
                mainsc.GetComponent<AudioSource>().PlayOneShot(mainsc.GetComponent<Main>().bingo);
                mainsc.GetComponent<Main>().foodright.SetActive(true);
                mainsc.GetComponent<Main>().foodwrong.SetActive(false);
                mainsc.GetComponent<Main>().showtime = 0;
            }
            else
            {
                this.transform.localPosition = orgpos;
                mainsc.GetComponent<AudioSource>().PlayOneShot(mainsc.GetComponent<Main>().wrong);
                mainsc.GetComponent<Main>().foodright.SetActive(false);
                mainsc.GetComponent<Main>().foodwrong.SetActive(true);
                mainsc.GetComponent<Main>().showtime = 0;
            }
            touch = false;
        }
    }
    void OnMouseDown()
    {
        if (mainsc.GetComponent<Main>().click == false)
        {
            this.GetComponent<SpriteRenderer>().sortingOrder = 2;
            mainsc.GetComponent<Main>().offset = this.gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f));
            mainsc.GetComponent<Main>().clickob = this.gameObject;
            mainsc.GetComponent<Main>().click = true;
        }
    }

    void OnMouseDrag()
    {
        
    }
    void OnMouseUp()
    {
        this.GetComponent<SpriteRenderer>().sortingOrder = 1;
        mainsc.GetComponent<Main>().click = false;
        this.transform.localPosition = orgpos;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "basket")
        {
            touch = true;
        }
    }
}
